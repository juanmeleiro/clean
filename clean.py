#!/usr/bin/python3
import argparse
import glob
import json
import sys
import os

CONFIG_PATH = os.path.expanduser('~/.clean/')
PATTERN_FILE = CONFIG_PATH + "patterns.json"

parser = argparse.ArgumentParser(description='Clean up directories of undersired files')
parser.add_argument('-p', '--pretend', action='store_true', help="In pretend\
        mode, the command only indicates what files would be deleted, but\
        doesn't actually do it")
parser.add_argument('-v', '--verbose',
        action = 'store_true',
        help = 'Enables verbose mode.')
parser.add_argument(
        'set',
        metavar='SET',
        help='Specifies the pattern-set to be used.')

if __name__ == '__main__':

    # Load patterns file
    with open(PATTERN_FILE) as f:
        patterns = json.load(f)

    # Parse command line options
    args = parser.parse_args()

    # If there is a set of patterns named args.set, find files that match it
    if args.set in patterns:
        matched_files = []
        for p in patterns[args.set]:
            matched_files.extend(glob.glob(p))
    else:
        print("There's no set of patterns named " + args.set)
        sys.exit(1)

    for f in matched_files:
        if args.pretend or args.verbose:
            print(f)
        if not args.pretend:
            os.remove(f)
