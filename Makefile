BIN_PATH=/usr/bin
PATTERNS_PATH=$(HOME)/.clean

install :
	sudo -u $(SUDO_USER) mkdir -p $(HOME)/.clean
	sudo -u $(SUDO_USER) cp patterns.json $(HOME)/.clean
	cp clean.py $(BIN_PATH)/clean

uninstall :
	rm -f $(BIN_PATH)/clean
	rm -rf $(HOME)/.clean
